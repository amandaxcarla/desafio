import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class desafio {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver", "C:/Driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void realizarlogin() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[1]/td/input")).sendKeys("amandaxcarla@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[2]/td/input")).sendKeys("autentication");
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[3]/td/textarea")).sendKeys("Enter in my acoount ");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[2]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(5000);
        Assert.assertEquals("Processed Form Details", driver.findElement(By.xpath("/html/body/div/h1")).getText());

    }

}
